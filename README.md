Three.js 

es una biblioteca de JavaScript que se utiliza para crear gráficos 3D 

https://threejs.org/ 


react-three-fiber 

es una biblioteca de enlace (wrapper) de React para Three.js. 
En otras palabras, simplifica la integración de la biblioteca Three.js 
en aplicaciones React al proporcionar componentes y abstracciones que 
facilitan la creación de gráficos 3D en el navegador web.

https://docs.pmnd.rs/react-three-fiber/getting-started/introduction


react-three-drei 

es otra biblioteca de React que se enfoca en complementar react-three-fiber 
y simplifica aún más el desarrollo de aplicaciones web con gráficos 3D utilizando Three.js. 
Proporciona una serie de componentes y utilidades adicionales para facilitar tareas 
comunes al trabajar con escenas 3D.

https://github.com/pmndrs/drei

RecoilJS

Se utiliza para gestionar el estado de una aplicación React de una manera eficiente y 
escalable

https://recoiljs.org/

open AI

La API de OpenAI permite a los desarrolladores enviar solicitudes a los servidores 
de OpenAI para realizar tareas de procesamiento de lenguaje natural, 
como generación de texto, traducción, resumen de texto, y más.

https://www.npmjs.com/package/openai

https://openai.com/

Canvas

ventana o espacio de representación para la escena 3D creada con Three.js
Este componente se utiliza para encapsular y renderizar elementos 3D
Dentro de él, puedes agregar luces, cámaras, y otros elementos de la escena

mesh

se refiere a un objeto tridimensional que define la geometría y los materiales 
de un objeto en la escena 3D. En otras palabras, es la representación visual 
de un objeto en el espacio tridimensional.

boxGeometry

es una clase que define la geometría de un cubo tridimensional

meshStandardMaterial

es un tipo de material utilizado en Three.js para definir cómo se verá un objeto tridimensional