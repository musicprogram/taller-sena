import {OPENAI_API_KEY} from "./key.js";

export const postConfigOpenAi = (prompt) => {
  const data = {
    method: "POST",
    headers : {
      'Content-type': "application/json",
      Authorization: `Bearer ${OPENAI_API_KEY}`,
      "User-Agent": "Chrome"
    },
    body : JSON.stringify({
      prompt: prompt,
      n: 1,
      size: '1024x1024'
    })
  }

  return data
}